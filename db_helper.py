import sqlalchemy
from sqlalchemy import orm
from sqlalchemy.sql import func
from sqlalchemy.sql.expression import label

from models import (LexicalUnit, Synset, UnitSynset)

def get_english_synsets():
    engine = create_alchemy_engine()
    session = create_alchemy_session(engine)
    query_result = find_synsets(session)
    synsets = []
    for synset in query_result:
        synsets.append(synset.id)
    print("zakończono")


def find_synsets(session, pos=None):
    if not pos:
        pos = [6] #we want find only english synsets. English synsets and units have pos > 4
    return (session.query(Synset.id, label("lex_ids", func.group_concat(UnitSynset.lex_id)),
                          label('unitindexes', func.group_concat(UnitSynset.unitindex))
                          )
            .join(UnitSynset)
            .join(LexicalUnit)
            .filter(LexicalUnit.pos.in_(pos))
            .order_by(Synset.id)
            )

def create_alchemy_engine():
    # TODO przerzucić to do jakiego pliku konfiguracyjnego
    settings = {'drivername': 'mysql', 'host': 'localhost', 'port': '3306', 'database': 'wordnet_work', 'username': 'root', 'password': 'root', 'query': {'charset': 'utf8mb4'}}
    url = sqlalchemy.engine.url.URL(**settings)
    engine = sqlalchemy.create_engine(url, echo=False)
    return engine

def create_alchemy_session(engine):
    session = sqlalchemy.orm.sessionmaker(bind=engine)
    return session()