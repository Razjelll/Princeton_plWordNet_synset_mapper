from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class LexicalUnit(Base):

    __tablename__ = 'lexicalunit'

    id = Column('id', Integer, primary_key=True)
    lemma = Column(String(255))
    pos = Column(Integer)

class Synset(Base):

    __tablename__ = 'synset'
    id = Column('id', Integer, primary_key=True)

class UnitSynset(Base):

    __tablename__ = 'unitandsynset'

    lex_id = Column(Integer, ForeignKey('lexicalunit.id'))
    syn_id = Column(Integer, ForeignKey('synset.id'), primary_key=True)
    unitindex = Column(Integer, primary_key=True)